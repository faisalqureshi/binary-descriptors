# Faisal Qureshi
# faisal.qureshi@uoit.ca
# http://vclab.science.uoit.ca

import cv2
import os
import argparse
import numpy as np
from datetime import datetime

def find_images(img_dir):
    dir_path = os.path.abspath(img_dir)

    for (root, dirs, files) in os.walk(dir_path):
        for name in files:
            yield os.path.join(root, name)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Extract binary descriptors from image files'
    )
    parser.add_argument('-d','--debug', dest='debug', action='store_true', help='debug flag')
    parser.add_argument('-v','--verbose', dest='verbose', action='store_true', help='be verbose')
    parser.add_argument('-s','--silent', dest='silent', action='store_true', help='suppress image showing during processing')
    parser.add_argument('-m','--method', dest='method', action='store', help='choose a binary descriptor: BRIEF (256bit), ORB, BRISK (512bit), default is BRIEF', default='brief')
    parser.add_argument('-n','--number', dest='nvec', type=int, action='store', default=1000, help='stop after collecting at least this many binary vectors. default is 1000.')
    parser.add_argument('-r','--restart', dest='restart', action='store', default='', help='full filename from which to start from in case of a restart')
    parser.add_argument('imgdir', help='directory containing image files')
    parser.add_argument('output', help='name for output file storing numpy uint8 vectors (binary vectors)')
    args = parser.parse_args()

    if not args.method.upper() in ['BRIEF', 'ORB', 'BRISK']:
        print 'Unknown method, please select either BRIEF or ORB'
        parser.print_help()
        exit(-1)

    if args.verbose:
        print 'Opening directory', args.imgdir

    process_files = True
    if os.path.isfile(args.restart):
        process_files = False

    out_file_i = 0
    t_elapsed = 0
    b_vec = None
    n_vec_needed = args.nvec
    n_vec_generated = 0
    first_msg = True
    t_msg_elapse = 10

    image_files = find_images(args.imgdir)
    for image_file in image_files:

        t_itr_start = datetime.now()

        if process_files:
            if args.verbose:
                print 'Processing file', image_file,
        else:
            if image_file == args.restart:
                process_files = True
                if args.verbose:
                    print 'Processing file', image_file,
            else:
                if args.verbose:
                    print 'Ignoring file', image_file
                continue

        img = cv2.imread(image_file, 0)
        if img == None:
            if args.verbose:
                print '... ERROR'
            continue
        else:
            if args.verbose:
                print '... OK'

        kp, des = None, None
        if args.method.upper() == 'BRIEF':
            star = cv2.FeatureDetector_create('STAR')
            kp = star.detect(img, None)
            brief = cv2.DescriptorExtractor_create('BRIEF')
            kp, des = brief.compute(img, kp)
        elif args.method.upper() == 'ORB':
            orb = cv2.ORB()
            kp = orb.detect(img, None)
            kp, des = orb.compute(img, kp)
        elif args.method.upper() == 'BRISK':
            fast = cv2.FastFeatureDetector()
            kp = fast.detect(img, None)
            brief = cv2.DescriptorExtractor_create("BRISK")
            kp, des = brief.compute(img, kp)
        else:
            print 'Unknown method, see help'
            break

        if args.debug:
            print 'kp', len(kp),
            if len(kp) > 0:
                print 'des', des.shape, des.dtype,
            if not b_vec == None:
                print 'b_vec', b_vec.shape, b_vec.dtype

        if len(kp) < 1:
            if args.verbose:
                print 'No descriptors computed'
        else:
            if args.verbose:
                print 'Descriptors computed', len(kp), des.shape[1]*8, 'bit'

            if b_vec == None:
                b_vec = np.empty((n_vec_needed, des.shape[1]), dtype=des.dtype)

            n_vec_to_copy = min(len(kp), n_vec_needed - n_vec_generated)
            b_vec[n_vec_generated:n_vec_generated+n_vec_to_copy,:] = des[0:n_vec_to_copy,:]
            n_vec_generated += n_vec_to_copy

        if not args.silent:
            img2 = cv2.drawKeypoints(img, kp, color=(0,255,0), flags=0)
            cv2.imshow('BVI Image', img2)

            key = cv2.waitKey(500)
            if key > 256:
                key = key % 256
            if key == ord('q'):
                break

        if n_vec_generated >= n_vec_needed:
            print 'Collected', n_vec_generated, 'binary vectors. Stopping.'
            break

        t_itr_end = datetime.now()
        t_itr = t_itr_end - t_itr_start
        t_elapsed += t_itr.microseconds
        t_ave = float(t_elapsed) / max(1, n_vec_generated)

        if args.silent:
            if first_msg:
                first_msg = False
                t_last_msg = t_itr_end
                print 'Generated', n_vec_generated, 'Time remaining', (n_vec_needed-n_vec_generated)*t_ave/1e6, 'seconds.'
            else:
                t_msg = t_itr_end - t_last_msg
                if t_msg.seconds > t_msg_elapse:
                    t_last_msg = t_itr_end
                    print 'Generated', n_vec_generated, 'Time remaining', (n_vec_needed-n_vec_generated)*t_ave/1e6, 'seconds.'

    if n_vec_generated > 0:
        if args.verbose or args.silent:
            print 'Saving', n_vec_generated, 'vectors to file', args.output
        np.save(args.output, b_vec[:n_vec_generated,:])
    else:
        print 'No vectors were generated.'